export default {
  props: {
    category: {},
    managesIndex: {},
    editable: {
      default: false
    },
  },
}

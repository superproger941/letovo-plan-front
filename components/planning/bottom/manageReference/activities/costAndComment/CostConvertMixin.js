import convertActivityValue from "~/helpers/convertActivityValue";

export default {
  computed: {
    convertValue() {
      return convertActivityValue(this.activity)
    }
  }
}

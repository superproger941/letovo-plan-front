import { tests } from "~/store/tests";
import { createPage, setupTest } from "@nuxt/test-utils";
import axios from 'axios'

import Comment from '@/components/planning/bottom/manageReference/activities/costAndComment/Comment'

const { Nuxt, Builder } = require('nuxt')

export const addVuetify = (context) => {
  context.vuetify = require('vuetify')
  context.vue.use(context.vuetify)
  // eslint-disable-next-line new-cap
  context.vuetifyInstance = new context.vuetify()
}

export const addVuex = (context) => {
  context.vuex = require('vuex')
  context.vue.use(context.vuex)
}

export const addFilter = (name, lambda) => {
  return context => context.vue.filter(name, lambda)
}

export const compositeConfiguration = (...configs) => {
  return context => configs.forEach(config => config(context))
}

export const bootstrapVueContext = (configureContext) => {
  const context = {}
  const teardownVueContext = () => {
    jest.unmock('vue')
    Object.keys(context).forEach(key => delete context[key])
    jest.resetModules()
  }

  jest.isolateModules(() => {
    context.vueTestUtils = require('@vue/test-utils')
    context.vue = context.vueTestUtils.createLocalVue()

    jest.doMock('vue', () => context.vue)

    configureContext && configureContext(context)
  })

  return {
    teardownVueContext,
    ...context
  }
}

let vueContext = null

describe('Logo', () => {

  let NuxtStore;
  let indexPage;

  beforeAll(async () => {
  })

  let nuxt = null

  beforeEach(async () => {
    vueContext = bootstrapVueContext(
      compositeConfiguration(addVuex, addVuetify)
    )
  })

  afterEach(() => {
    vueContext.teardownVueContext()
  })

  setupTest({
    fixture: 'fixtures/consola',
    server: true,
    browser: true,
  })

  test('Test Logo Component', async () => {
    const page = await createPage('/')
    await page.waitForFunction('!!window.$nuxt')

    const { user, axiosBearer, response } = await page.evaluate(
      async () => {
        const response = await window.$nuxt.$auth.loginWith('local', {
          data: { username: 'vladislav.grigorev', password: 'Pox81504' }
        })

        const strategy = window.$nuxt.$auth.strategy

        return {
          axiosBearer: window.$nuxt.$axios.defaults.headers.common.Authorization,
          // token: strategy.token.get(),
          user: window.$nuxt.$auth.user,
          response,
        }
      }
    )
    let ManageReferences = require('../../components/planning/bottom/ManageReferences.vue').default;

    tests.$axios = axios
    tests.$axios.defaults.baseURL = process.env.BASE_URL || 'http://localhost:8000/api'
    tests.$axios.defaults.headers.common.Authorization = axiosBearer

    const planningWrapper = vueContext.vueTestUtils.shallowMount(ManageReferences, {
      localVue: vueContext.vue,
      vuetify: vueContext.vuetifyInstance,
      store: tests
    })

    planningWrapper.vm.$root.$on('year-selected', async (yearsStart) => {
      await planningWrapper.vm.$store.dispatch('planning/getPage', { id: 6797, yearsStart })

      let manageReferences = planningWrapper.vm.$store.getters["planning/manageReferences"]
      let manageReference = manageReferences.shift()
      let activity = manageReference.activities.shift()

      const wrapperComment = vueContext.vueTestUtils.shallowMount(Comment, {
        localVue: vueContext.vue,
        store: tests,
        propsData: {
          managesIndex: 0,
          activityIndex: 0,
          activity,
          disabled: false,
          saved: false,
          editable: false,
        }
      })

      let comment = wrapperComment.find('.comment')

      console.log('comment', comment.html());
      //expect(wrapperComment.html()).toMatch('Logo')
    })

    await planningWrapper.vm.$store.dispatch('years/getYears')

    planningWrapper.vm.$root.$emit('year-selected', (planningWrapper.vm.$store.getters['years/getCurrentYear']).years_start)
  }, 99999)
})

import Vue from 'vue'
import activitiesCheckedSum from "~/helpers/store/planning/activitiesCheckedSum";
import checkResponseError from "~/helpers/checkResponseError";
import showHideContent from "~/helpers/store/planning/showHideContent";
import clickActivity from "~/helpers/store/planning/clickActivity";
import showHideSaved from "~/helpers/store/planning/showHideSaved";

export const state = () => ({
  manageReferences: [],
})

export const actions = {
  async getPage(context, { id, yearsStart }) {
    try {
      const res = await this.$axios.get(`students/${id}/getPlanningPage?years_start=${yearsStart}`)

      let manageReferences = [];

      for (let index in res.data) {
        manageReferences.push(res.data[index])
      }

      context.commit('setManageReferences', manageReferences)
    } catch (error) {
      if (checkResponseError(error.response.status)) {
        await this.$router.push('/home')
      }
    }
  },
  async saveActivityCategoryForStudent(context, { managesIndex, id, yearsStart }) {
    try {
      const filteredManageReferences = context.state.manageReferences.map((item) => {
        return item.activities.filter((obj) => {
          return !!obj.checked
        })
      })

      const res = await this.$axios.post(`students/${id}/saveActivities?year_id=${yearsStart}`, {
        activities: filteredManageReferences,
      })

      context.commit('saveActivityCategory', managesIndex)
    } catch (error) {
      if (checkResponseError(error.response.status)) {
        await this.$router.push('/home')
      }
    }
  },
  showHideContent({ commit, getters }, { index, value }) {
    commit('showHideContent', { index, value, getters })
  },
  showHideSaved({ commit, getters }, { index, value }) {
    commit('showHideSaved', { index, value, getters })
  }
}

export const mutations = {
  setManageReferences(state, categories) {
    Vue.set(state, 'manageReferences', categories)
  },
  showHideContent(state, { index, value, getters }) {
    showHideContent(index, value, getters)
  },
  showHideSaved(state, { index, value, getters }) {
    showHideSaved(index, value, getters)
  },
  clickActivity(state, { checked, managesIndex, activityIndex }) {
    const mrs = state.manageReferences

    Vue.set(mrs, managesIndex, clickActivity(mrs[managesIndex], activityIndex, checked))
  },
  saveComment(state, { value, managesIndex, activityIndex }) {
    const obj = Object.assign({}, state.manageReferences[managesIndex])

    obj['activities'][activityIndex]['comment'] = value
    Vue.set(state.manageReferences, managesIndex, obj)
  },
  subActivityCostCount(state, { managesIndex, activityIndex }) {
    const obj = Object.assign({}, state.manageReferences[managesIndex])
    const constant = obj['activities'][activityIndex]['constant_count'];

    let count = obj['activities'][activityIndex]['count'];

    if (count > 1) {
      if (constant) {
        if (count > constant) {
          obj['activities'][activityIndex]['count']--;
        }
      } else {
        obj['activities'][activityIndex]['count']--;
      }
    }

    Vue.set(state.manageReferences, managesIndex, obj)
  },
  addActivityCostCount(state, { managesIndex, activityIndex }) {
    const obj = Object.assign({}, state.manageReferences[managesIndex])

    obj['activities'][activityIndex]['count']++;
    Vue.set(state.manageReferences, managesIndex, obj)
  },
  async saveActivityCategory(state, index) {
    Vue.set(state.manageReferences[index], 'contentShow', false)
    state.manageReferences[index].activities.map((element) => {
      element.saved = element.checked

      return element
    })
  },
}

export const getters = {
  manageReferences: (state) => {
    return state.manageReferences
  },
  activitiesCheckedSum: state => index => {
    return activitiesCheckedSum(state.manageReferences, index)
  },
  hasAnyActivitySaved: state => index => {
    const activities = state.manageReferences[index].activities ? state.manageReferences[index].activities : [];

    return activities.find((element) => {
      return element.saved === true
    })
  },
  anyActivitiesChecked: state => index => {
    const res = state.manageReferences[index]['activities'].find((element) => {
      return element.checked === true;
    })

    return res !== undefined
  }
}

export const planning = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};

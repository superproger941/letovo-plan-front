import Vue from 'vue'

export const state = () => ({
  students: []
})

export const actions = {
  async getStudents(context, {id}) {
    try {
      const res = await this.$axios.get(`students`)

      context.commit('setStudents', res.data.data.students)
    } catch (error) {
      console.log('error', error);
      context.commit('setStudents', [])
    }
  }
}

export const mutations = {
  setStudents(state, students) {
    students.forEach((student) => {
      student.manages = {};
      if (student.activities_with_manages_reference !== undefined && student.activities_with_manages_reference.length > 0) {
        student.activities_with_manages_reference.forEach((activity) => {
          if (!student.manages.hasOwnProperty(activity.manage_reference.title)) {
            student.manages[activity.manage_reference.title] = [];
            student.manages[activity.manage_reference.title]['count'] = 0;
            student.manages[activity.manage_reference.title]['color'] = activity.manage_reference.color;
          }

          student.manages[activity.manage_reference.title]['count'] += activity.pivot.count * activity.pivot.value;
        });
      }
    })
    Vue.set(state, 'students', students)
  },
}

export const getters = {
  students: (state) => {
    return state.students;
  },
}

export const home = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};

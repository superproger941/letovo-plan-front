export const state = () => ({
  years: [],
  currentYear: null
})

export const actions = {
  async getYears(context) {
    try {
      const res = await this.$axios.get(`years`)

      context.commit('setYears', res.data)
    } catch (error) {
      console.log('error', error);
    }
  },
}

export const mutations = {
  setYears(state, years) {
    years = years.map((year) => {
      year.old = (year.years_end < new Date().getFullYear())
      year.selected = year.current = (year.years_end === new Date().getFullYear())

      if (year.current) {
        state.currentYear = year
      }

      return year
    })

    state.years = years
  },
  setTabSelected(state, index) {
    state.years.map((item) => {
      item.selected = false

      return item
    })

    state.years[index].selected = true
  },
}

export const getters = {
  years: (state) => {
    return state.years;
  },
  getSelectedYear: (state) => {
    const year = state.years.find((item) => {
      return !!item.selected
    })

    return year ? year : {}
  },
  getCurrentYear: (state) => {
    return state.currentYear
  },
}

export const years = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};

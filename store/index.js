import Vue from 'vue'

export const state = () => ({
  hasPrevPage: false,
  background: `url(/images/background.jpg)`,
  validationErrors: {}
})

export const actions = {
  async login(context, { username, password }) {
    try {
      await this.$auth.loginWith('local', {
        data: {
          username,
          password
        }
      })
      await this.$router.push('/home')
    } catch (error) {
      console.log('error', error);
      if (error.response.status === 422) {
        context.commit('setValidationErrors', error.response.data.errors)
      }
    }
  }
}

export const mutations = {
  setHasPrevPage(state, has) {
    state.hasPrevPage = has;
  },
  setBackgroundImage(state, backgroundImage) {
    state.background = backgroundImage
  },
  setValidationErrors(state, errors) {
    Vue.set(state, 'validationErrors', errors)
  }
}

export const getters = {
  hasPrevPage: (state) => {
    return !!state.hasPrevPage;
  },
  background: (state) => {
    return state.background;
  },
  validationErrors: (state) => {
    return state.validationErrors;
  },
  isAuthenticated(state) {
    return state.auth.loggedIn
  },
  loggedInUser(state) {
    return state.auth.user
  },
  user(state) {
    return state.auth.user
  }
}

export const index = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};

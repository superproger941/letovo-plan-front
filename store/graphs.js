import Vue from "vue";
import checkResponseError from "~/helpers/checkResponseError";

export const state = () => ({
  graphs: [],
})

export const actions = {
  async getGraphsRequest(context, id) {
    try {
      const res = await this.$axios.get(`students/${id}/getGraphs`)

      context.commit('setGraphs', res.data)
    } catch (error) {
      console.log('error', error);

      if (checkResponseError(error.response.status)) {
        await this.$router.push('/home')
      }
    }
  },
}

export const mutations = {
  setGraphs(state, graphs) {
    Vue.set(state, 'graphs', graphs)
  },
}

export const getters = {
  getGraphs: (state) => {
    return state.graphs
  },
}

export const graphs = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};

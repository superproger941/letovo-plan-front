import Vue from 'vue';
import Vuex from 'vuex';

import { planning } from './planning';
import { index } from './index';
import { home } from './home';
import { graphs } from './graphs';
import { years } from './years';
import { students } from './students';

Vue.use(Vuex);

export const tests = new Vuex.Store({
    modules: {
      planning,
      index,
      home,
      graphs,
      years,
      students
    }
});

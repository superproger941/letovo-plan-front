import checkResponseError from "~/helpers/checkResponseError";

export const state = () => ({
  student: {},
})

export const actions = {
  async getStudentById(context, id) {
    try {
      const res = await this.$axios.get(`students/${id}`)

      context.commit('setStudent', res.data)
    } catch (error) {
      console.log('error', error);

      if (checkResponseError(error.response.status)) {
        await this.$router.push('/home')
      }
    }
  },
}

export const mutations = {
  setStudent(state, student) {
    state.student = student
  },
}

export const getters = {
  student: (state) => {
    return state.student
  }
}

export const students = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};

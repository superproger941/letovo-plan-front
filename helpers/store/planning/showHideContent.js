export default function showHideContent(index, value, getters) {
  const mrs = getters.manageReferences
  const mr = getters.manageReferences[index]

  mrs.map((item) => {
    item.contentShow = false

    return item
  })

  mr.contentShow = value
}

export default function showHideSaved(index, value, getters) {
  const mrs = getters.manageReferences
  const mr = getters.manageReferences[index]

  mrs.map((item) => {
    item.savedShow = false

    return item
  })

  if (mr) {
    mr.savedShow = value
  }
}

export default function clickActivity(manageReference, activityIndex, checked) {
  const obj = Object.assign({}, manageReference)

  obj['activities'][activityIndex]['checked'] = checked

  if (obj['activities'][activityIndex]['count'] === 0 && !obj['activities'][activityIndex]['constant_count']) {
    obj['activities'][activityIndex]['count'] ++
  }

  return obj
}

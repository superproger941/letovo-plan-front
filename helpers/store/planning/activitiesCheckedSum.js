import convertActivityValue from "~/helpers/convertActivityValue";

export default function activitiesCheckedSum(manageReferences, index) {
  return manageReferences.map((item) => {
    const activities = item.activities ? item.activities : [];

    return calculateManageReferenceActivitiesSum(activities)
  })[index]
}

function calculateManageReferenceActivitiesSum(activities) {
  return activities.reduce((acc, val) => {
    const constantCnt = val.constant_count ? val.constant_count : 0
    const convertedVal = convertActivityValue(val)
    const countOfValues = (val.checked ? parseInt(val.count + constantCnt) * convertedVal : 0)

    return acc + countOfValues
  }, 0)
}

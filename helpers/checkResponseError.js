export default function checkResponseError(status) {
  return status === 401 || status === 404 || status === 429
}

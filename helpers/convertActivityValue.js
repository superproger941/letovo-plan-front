export default function convertActivityValue(activity) {
  switch (activity.medal_level) {
    case 'Бронзовые':
      return activity.value;
    default:
      return activity.value * 2;
  }
}
